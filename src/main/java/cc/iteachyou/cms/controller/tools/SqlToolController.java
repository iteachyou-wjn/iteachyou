package cc.iteachyou.cms.controller.tools;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.sql.SQLUtils;

import cc.iteachyou.cms.common.ResponseResult;
import cc.iteachyou.cms.common.StateCodeEnum;
import cn.hutool.core.util.StrUtil;

/**
 * Html工具类
 * @author Jonas
 *
 */
@RestController
@RequestMapping("tools/sql")
public class SqlToolController {
	
	/**
	 * 格式化Json
	 * @param params
	 * @return
	 */
	@PostMapping("format")
	public ResponseResult format(@RequestBody Map<String,String> params) {
		ResponseResult result = null;
		if(!params.containsKey("sql") || StrUtil.isBlank(params.get("sql"))) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
		String dbType = "";
		if(!params.containsKey("dbType") || StrUtil.isBlank(params.get("dbType"))) {
			dbType = "MYSQL";
		}else {
			dbType = params.get("dbType");
		}
		try {
			String string = SQLUtils.format(params.get("sql"), dbType);
			result = ResponseResult.Factory.newInstance(Boolean.TRUE, StateCodeEnum.HTTP_SUCCESS.getCode(), string, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		} catch (Exception e) {
			result = ResponseResult.Factory.newInstance(Boolean.FALSE, StateCodeEnum.HTTP_SUCCESS.getCode(), null, StateCodeEnum.HTTP_SUCCESS.getDescription());
			return result;
		}
	}
	
}
